package ru.sgtraf.fizfaktabs.fragments;

import ru.sgtraf.fizfaktabs.R;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;


public class ScreenPref extends Fragment implements TextWatcher {

    private EditText userNameEditText, userSurnameEditText, phoneEditText, emailEditText,
            FBEditText, VKEditText, TWEditText;

    private SharedPreferences sPref;

    private NumberPicker np, np2;
    private Spinner FacultetText;

    public ScreenPref() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.screen_pref, container, false);

        //находим элементы разметки

        userNameEditText = (EditText) rootView.findViewById(R.id.userNameEditText);
        userSurnameEditText = (EditText) rootView.findViewById(R.id.userSurnameEditText);
        //	phoneEditText = (EditText) rootView.findViewById(R.id.phoneEditText);
        emailEditText = (EditText) rootView.findViewById(R.id.emailEditText);
        FBEditText = (EditText) rootView.findViewById(R.id.FBEditText);
        VKEditText = (EditText) rootView.findViewById(R.id.VKEditText);

        FacultetText = (Spinner) rootView.findViewById(R.id.spinner);


        //	TWEditText = (EditText) rootView.findViewById(R.id.TWEditText);
//		enterKursText = (EditText) rootView.findViewById(R.id.enterKursText);
//		enterGroupText = (EditText) rootView.findViewById(R.id.enterGroupText);

        np = (NumberPicker) rootView.findViewById(R.id.numberPicker1);
        np.setMaxValue(4);
        np.setMinValue(1);

        np2 = (NumberPicker) rootView.findViewById(R.id.numberPicker2);
        np2.setMaxValue(6);
        np2.setMinValue(1);

        // здесь устанавливаем сллушателя
        /*
		userNameEditText.addTextChangedListener(this);
		userSurnameEditText.addTextChangedListener(this);
		phoneEditText.addTextChangedListener(this);
		emailEditText.addTextChangedListener(this);
		FBEditText.addTextChangedListener(this);
		VKEditText.addTextChangedListener(this);
		TWEditText.addTextChangedListener(this);
		enterKursText.addTextChangedListener(this);
		enterGroupText.addTextChangedListener(this);

*/
        //здесь заполняем данными разметку

        sPref = getActivity().getPreferences(getActivity().MODE_PRIVATE);
        //  String savedText = sPref.getString(SAVED_TEXT, "");
        userNameEditText.setText(sPref.getString("userNameEditText", ""));
        userSurnameEditText.setText(sPref.getString("userSurnameEditText", ""));
        //	    phoneEditText.setText(sPref.getString("phoneEditText", ""));
        emailEditText.setText(sPref.getString("emailEditText", ""));
        FBEditText.setText(sPref.getString("FBEditText", ""));
        VKEditText.setText(sPref.getString("VKEditText", ""));
        //	    TWEditText.setText(sPref.getString("TWEditText", ""));
        //  enterKursText.setText(sPref.getString("enterKursText", "1"));
        //  enterGroupText.setText(sPref.getString("enterGroupText", "1"));

        np.setValue((int) sPref.getLong("enterKursText", 1));
        np2.setValue((int) sPref.getLong("enterGroupText", 1));
        FacultetText.setSelection(sPref.getInt("FacultetText", 1));

      //  Log.i("onCreateView", "считано: "); // Oops

        //	    Toast.makeText(this, "Text loaded", Toast.LENGTH_SHORT).show();


        //один из способов обработки действий
			/*	 	
		enterGroupText.addTextChangedListener(
				new TextWatcher() {

            public void afterTextChanged(Editable s) {
                //do some validation and then TOAST 
            	
            	if (Integer.parseInt(enterGroupText.getText().toString())>6)
            	{
            		Toast.makeText(getActivity(), "лопух", Toast.LENGTH_SHORT).show();
            	}
				
            	
            	

            }

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
        });
		*/


        return rootView;
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub


        //    Toast.makeText(this, "Text saved", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();

        // сохраняем данные

        sPref = getActivity().getPreferences(getActivity().MODE_PRIVATE);
        Editor ed = sPref.edit();
        ed.putString("userNameEditText", userNameEditText.getText().toString());
        ed.putString("userSurnameEditText", userSurnameEditText.getText().toString());
//	    ed.putString("phoneEditText", phoneEditText.getText().toString());
        ed.putString("emailEditText", emailEditText.getText().toString());
        ed.putString("FBEditText", FBEditText.getText().toString());
        ed.putString("VKEditText", VKEditText.getText().toString());

        ed.putInt("FacultetText", FacultetText.getSelectedItemPosition());

//	    ed.putString("TWEditText", TWEditText.getText().toString());
        //   ed.putString("enterKursText", enterKursText.getText().toString());
        //   ed.putString("enterGroupText", enterGroupText.getText().toString());

        ed.putLong("enterKursText", np.getValue());
        ed.putLong("enterGroupText", np2.getValue());


        ed.apply();
       // Log.i("afterTextChanged", "записано: "); // Oops
        //	Toast.makeText(getActivity(), "Данные сохранены", Toast.LENGTH_SHORT).show();


    }


}

package ru.sgtraf.fizfaktabs;

import java.util.Calendar;

/**
 * Created by gad on 15.02.2017.
 */


// Получаем текущую неделю
public class WeekInfo {
    public static int getWeek() {
        return Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
    }


    // Получаем номер недели 1 сентября
    public static int getWeekOffirstSeptember(int year) {

        Calendar cal = Calendar.getInstance();
        cal.set(year, 8, 1);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

//Общее количество недель в году

    public static int getNumberAllWeekInYear(int year) {

        Calendar cal = Calendar.getInstance();
        cal.set(year, 11, 25);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }


    //Делаем расчет текущей учебной недели

    public static int getNumberOfCurrentWeek() {

        int month = Calendar.getInstance().get(Calendar.MONTH);
int year1 = Calendar.getInstance().get(Calendar.YEAR);

        if (month < 8) {

            int n = 0;

//тут мы просто считаем номер текущей недели от 1 сентября
            n = (getNumberAllWeekInYear(year1-1) - (getWeekOffirstSeptember(year1-1) - 1)) + getWeek();
//если она нечетная то это первая неделя
           /* if (n % 2 == 0) {
                return 2;
            } else {
                return 1;
            }*/

            return isOdd(n);


        } else {


            return isOdd(getWeek()-getWeekOffirstSeptember(year1)+1);
        }
    }

    public static int isOdd(int n) {

        if (n % 2 == 0) {
            return 2;
        } else {
            return 1;
        }
    }


}

package ru.sgtraf.fizfaktabs.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import ru.sgtraf.fizfaktabs.R;

/**
 * Created by gad on 20.02.2015.
 */
public class ScreenBells extends Fragment {

    public View rootView;
    public TextView textview;
    public TextView textview22;

    final int SEC = 1 * 1000;
    final int MIN = 60 * SEC;
    final int HOUR = 60 * MIN;
    private Handler handler;
    private String text;
    final String LOG_TAG = "myLogs";

    @Override
    public void onCreate(Bundle savedInstanceState) {


        handler = new Handler();
        handler.post(showTime);

        super.onCreate(savedInstanceState);
    }

    Runnable showTime = new Runnable() {
        public void run() {
            //   Log.d(LOG_TAG, "showInfo");

            textview.setText(calc()[0]);
            if (calc()[1] != null) {
                textview22.setTextSize(50);
                textview22.setText(calc()[1]);
            }


            handler.postDelayed(showTime, 1000);


            //   Log.d(LOG_TAG, calc()[1]);
            // Если счетчк закончил тикать останавливаем очередь сообщений

            if (calc()[1] == null) {
                textview22.setVisibility(View.GONE);
                handler.removeCallbacks(showTime);
            }


        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.screen_bells, container, false);

        textview = (TextView) rootView.findViewById(R.id.textView);
        textview22 = (TextView) rootView.findViewById(R.id.textView22);


        return rootView;

    }


    public String[] calc() {
        GregorianCalendar date = new GregorianCalendar();


        int hours = date.get(date.HOUR_OF_DAY); //date.getHours();
        int min = date.get(date.MINUTE); //date.getMinutes();
        int sec = date.get(date.SECOND); //date.getSeconds();
        String[] text = new String[2];
        //   String text = "Сейчас " + String.valueOf(hours) + ":" + String.valueOf(min);

//отрезки времени часов пар


        /*
//double brace initialization
        Map<String, GregorianCalendar> baida = new HashMap<String, GregorianCalendar>()
        {{
            put("date1start", new GregorianCalendar(0,0,0,8,30));
            put("date1end", new GregorianCalendar(0,0,0,9,15));
            put("date2start", new GregorianCalendar(0,0,0,9,20));
            put("date2end", new GregorianCalendar(0,0,0,10,5));
            put("date3start", new GregorianCalendar(0,0,0,10,15));
            put("date3end", new GregorianCalendar(0,0,0,11,0));
            put("date4start", new GregorianCalendar(0,0,0,11,5));
            put("date4end", new GregorianCalendar(0,0,0,11,50));
            put("date5start", new GregorianCalendar(0,0,0,12,20));
            put("date5end", new GregorianCalendar(0,0,0,13,5));
            put("date6start", new GregorianCalendar(0,0,0,13,10));
            put("date6end", new GregorianCalendar(0,0,0,13,55));
            put("date7start", new GregorianCalendar(0,0,0,14,0));
            put("date7end", new GregorianCalendar(0,0,0,14,45));
            put("date8start", new GregorianCalendar(0,0,0,14,50));
            put("date8end", new GregorianCalendar(0,0,0,15,35));
        }};
*/

        final ArrayList hour1 = new ArrayList();
        hour1.add(new HashMap<String, GregorianCalendar>() {{
            put("date1start", new GregorianCalendar(0, 0, 0, 8, 30));
            put("date1end", new GregorianCalendar(0, 0, 0, 9, 15));
        }});

        final ArrayList hour2 = new ArrayList();
        hour2.add(new HashMap<String, GregorianCalendar>() {{
            put("date2start", new GregorianCalendar(0, 0, 0, 9, 20));
            put("date2end", new GregorianCalendar(0, 0, 0, 10, 5));
        }});

        final ArrayList hour3 = new ArrayList();
        hour3.add(new HashMap<String, GregorianCalendar>() {{
            put("date3start", new GregorianCalendar(0, 0, 0, 10, 15));
            put("date3end", new GregorianCalendar(0, 0, 0, 11, 0));
        }});

        final ArrayList hour4 = new ArrayList();
        hour4.add(new HashMap<String, GregorianCalendar>() {{
            put("date4start", new GregorianCalendar(0, 0, 0, 11, 5));
            put("date4end", new GregorianCalendar(0, 0, 0, 11, 50));
        }});

        final ArrayList hour5 = new ArrayList();
        hour5.add(new HashMap<String, GregorianCalendar>() {{
            put("date5start", new GregorianCalendar(0, 0, 0, 12, 20));
            put("date5end", new GregorianCalendar(0, 0, 0, 13, 5));
        }});

        // проверка правильности объявления, чтобы gradle ошибку не выдавал
        final ArrayList<HashMap<String, GregorianCalendar>> hour6 = new ArrayList<>();
        hour6.add(new HashMap<String, GregorianCalendar>() {{
            put("date6start", new GregorianCalendar(0, 0, 0, 13, 10));
            put("date6end", new GregorianCalendar(0, 0, 0, 13, 55));
        }});

        final ArrayList hour7 = new ArrayList();
        hour7.add(new HashMap<String, GregorianCalendar>() {{
            put("date7start", new GregorianCalendar(0, 0, 0, 14, 0));
            put("date7end", new GregorianCalendar(0, 0, 0, 14, 45));
        }});

        final ArrayList hour8 = new ArrayList();
        hour8.add(new HashMap<String, GregorianCalendar>() {{
            put("date8start", new GregorianCalendar(0, 0, 0, 14, 50));
            put("date8end", new GregorianCalendar(0, 0, 0, 15, 35));
        }});

        Map<String, ArrayList<HashMap<String, GregorianCalendar>>> baida2 = new HashMap<String, ArrayList<HashMap<String, GregorianCalendar>>>();

        baida2.put("1", hour1);
        baida2.put("2", hour2);
        baida2.put("3", hour3);
        baida2.put("4", hour4);
        baida2.put("5", hour5);
        baida2.put("6", hour6);
        baida2.put("7", hour7);
        baida2.put("8", new ArrayList() {{
            add(new HashMap<String, GregorianCalendar>() {{
                put("date8start", new GregorianCalendar(0, 0, 0, 14, 50));
                put("date8end", new GregorianCalendar(0, 0, 0, 15, 35));
            }});
        }});


//лучше  ArrayList не использовать, а сделать для каждого часа отдельный HashMap


        GregorianCalendar date1start = new GregorianCalendar();
        //  date1start.clear();
        date1start.set(Calendar.HOUR_OF_DAY, 8);
        date1start.set(Calendar.MINUTE, 30);

        GregorianCalendar date1end = new GregorianCalendar();
        //    date1end.clear();
        date1end.set(Calendar.HOUR_OF_DAY, 9);
        date1end.set(Calendar.MINUTE, 15);
        date1end.set(Calendar.SECOND, 0);

        GregorianCalendar date2start = new GregorianCalendar();
        //      date2start.clear();
        date2start.set(Calendar.HOUR_OF_DAY, 9);
        date2start.set(Calendar.MINUTE, 20);

        GregorianCalendar date2end = new GregorianCalendar();
        //      date2end.clear();
        date2end.set(Calendar.HOUR_OF_DAY, 10);
        date2end.set(Calendar.MINUTE, 5);
        date2end.set(Calendar.SECOND, 0);

        GregorianCalendar date3start = new GregorianCalendar();
        //      date3start.clear();
        date3start.set(Calendar.HOUR_OF_DAY, 10);
        date3start.set(Calendar.MINUTE, 15);

        GregorianCalendar date3end = new GregorianCalendar();
        //       date3end.clear();
        date3end.set(Calendar.HOUR_OF_DAY, 11);
        date3end.set(Calendar.MINUTE, 0);
        date3end.set(Calendar.SECOND, 0);

        GregorianCalendar date4start = new GregorianCalendar();
        //       date4start.clear();
        date4start.set(Calendar.HOUR_OF_DAY, 11);
        date4start.set(Calendar.MINUTE, 5);

        GregorianCalendar date4end = new GregorianCalendar();
        //       date4end.clear();
        date4end.set(Calendar.HOUR_OF_DAY, 11);
        date4end.set(Calendar.MINUTE, 50);
        date4end.set(Calendar.SECOND, 0);

        GregorianCalendar date5start = new GregorianCalendar();
        //       date5start.clear();
        date5start.set(Calendar.HOUR_OF_DAY, 12);
        date5start.set(Calendar.MINUTE, 20);

        GregorianCalendar date5end = new GregorianCalendar();
        //       date5end.clear();
        date5end.set(Calendar.HOUR_OF_DAY, 13);
        date5end.set(Calendar.MINUTE, 5);
        date5end.set(Calendar.SECOND, 0);

        GregorianCalendar date6start = new GregorianCalendar();
        //       date6start.clear();
        date6start.set(Calendar.HOUR_OF_DAY, 13);
        date6start.set(Calendar.MINUTE, 10);

        GregorianCalendar date6end = new GregorianCalendar();
        //      date6end.clear();
        date6end.set(Calendar.HOUR_OF_DAY, 13);
        date6end.set(Calendar.MINUTE, 55);
        date6end.set(Calendar.SECOND, 0);

        GregorianCalendar date7start = new GregorianCalendar();
        //      date7start.clear();
        date7start.set(Calendar.HOUR_OF_DAY, 14);
        date7start.set(Calendar.MINUTE, 0);

        GregorianCalendar date7end = new GregorianCalendar();
        //      date7end.clear();
        date7end.set(Calendar.HOUR_OF_DAY, 14);
        date7end.set(Calendar.MINUTE, 45);
        date7end.set(Calendar.SECOND, 0);
/*
        GregorianCalendar date8start = new GregorianCalendar();
        //       date8start.clear();
        date8start.set(Calendar.HOUR_OF_DAY, 14) ;
        date8start.set(Calendar.MINUTE, 50);

        GregorianCalendar date8end = new GregorianCalendar();
        //       date8end.clear();
        date8end.set(Calendar.HOUR_OF_DAY, 15) ;
        date8end.set(Calendar.MINUTE, 35);
        date8end.set(Calendar.SECOND, 0);
//отрезки времени часов пар
*/
        //    Log.d(LOG_TAG, baida.get("date8end").toString() );
        //    Log.d(LOG_TAG, date8end.toString() );


//обработка окончания занятий
        if (hours > 15) {
            text[0] = "Занятия к сожалению закончились :( ";
        }
        if ((hours == 15) & (min >= 35)) {
            text[0] = "Занятия к сожалению закончились :( ";
        }
        if ((hours < 8) | ((hours == 8) & (min < 30))) {
            text[0] = "Занятия к счастью еще не начались :) ";
        }


        //обработка окончания первого часа
        if (date.after(date1start) & date.before(date1end)) {
            long dif = date1end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
            //+ '\n' + String.valueOf(mins) + " : " + secs
        }

        //обработка окончания второго часа
        if (date.after(date2start) & date.before(date2end)) {
            long dif = date2end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
        }

        //обработка окончания 3 часа
        if (date.after(date3start) & date.before(date3end)) {
            long dif = date3end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
        }

        //обработка окончания 4 часа
        if (date.after(date4start) & date.before(date4end)) {
            long dif = date4end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
            //   Log.d(LOG_TAG, String.valueOf(mins) + " : " + String.valueOf(secs));

        }

        //обработка окончания 5 часа
        if (date.after(date5start) & date.before(date5end)) {
            long dif = date5end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
        }

        //обработка окончания 6 часа
        if (date.after(date6start) & date.before(date6end)) {
            long dif = date6end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
        }

        //обработка окончания 7 часа
        if (date.after(date7start) & date.before(date7end)) {
            long dif = date7end.getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
        }

        //map.get(8).get(date8start)
        //обработка окончания 8 часа
        if (date.after(baida2.get("8").get(0).get("date8start")) & date.before(baida2.get("8").get(0).get("date8end"))) {
            long dif = baida2.get("8").get(0).get("date8end").getTimeInMillis() - date.getTimeInMillis();
            long mins = dif / MIN;
            long secs = (dif - mins * MIN) / SEC;
            text[0] = "Перемена через";
            text[1] = String.valueOf(mins) + " : " + String.valueOf(secs);
        }


        return text;
    }

    @Override
    public void onResume() {
        handler.post(showTime);

        super.onResume();
    }

    @Override
    public void onPause() {
        handler.removeCallbacksAndMessages(null);
        handler.removeCallbacks(showTime);
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // http://habrahabr.ru/company/badoo/blog/240479/
        handler.removeCallbacksAndMessages(null);
        handler.removeCallbacks(showTime);
        //  Log.e(this.getClass().getName(), "Etesttest");

    }
}

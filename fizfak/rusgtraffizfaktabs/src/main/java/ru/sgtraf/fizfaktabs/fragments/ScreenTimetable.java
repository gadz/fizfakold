package ru.sgtraf.fizfaktabs.fragments;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import ru.sgtraf.fizfaktabs.R;
import ru.sgtraf.fizfaktabs.view.SlidingTabLayout;

public class ScreenTimetable extends Fragment {

	private SlidingTabLayout mSlidingTabLayout;
	private ViewPager mViewPager;
	private SharedPreferences sPref;
	private int Kurs, Group, Facultet;
	private TextView WhatIsKurs;
	public ScreenTimetable() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.screen_timetable, container,
				false);

		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// Get the ViewPager and set it's PagerAdapter so that it can display
		// items
		mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
		mViewPager.setAdapter(new SamplePagerAdapter());

		// Give the SlidingTabLayout the ViewPager, this must be
		// done AFTER the ViewPager has had it's PagerAdapter set.
		mSlidingTabLayout = (SlidingTabLayout) view
				.findViewById(R.id.sliding_tabs);
		// mSlidingTabLayout.setCustomTabView(R.layout.custom_tab_title,
		// R.id.tabtext);

		WhatIsKurs = (TextView) view.findViewById(R.id.WhatIsKurs);
		
		sPref = getActivity().getPreferences(getActivity().MODE_PRIVATE);
		
		// проверяем, установлена ли прога на телефоне ( первый ли раз открывается программа)
        boolean hasFirst = sPref.getBoolean("hasFirst", false);
		
        if (!hasFirst) {
            // стираем данные в настройках если они были до этого (необходимо при переходе от edittext на numberpick)
        	//выходила ошибка при обновлении, из за чтения строки - лонгом
        //	sPref.edit().clear();
            Editor e = sPref.edit();
            e.clear();
            e.commit(); // не забудьте подтвердить изменения
            e.putBoolean("hasFirst", true);
            e.commit();
        }
        
		
		//  Kurs =  Integer.parseInt(sPref.getString("enterKursText", "1"));
	//	  Group  =  Integer.parseInt(sPref.getString("enterGroupText", "1"));
		
		
		 Kurs =  (int)sPref.getLong("enterKursText", 1);
		 Group  =  (int)sPref.getLong("enterGroupText", 1);
		Facultet  =  sPref.getInt("FacultetText", 1);


		//		Toast.makeText(getActivity(), "курс "+Kurs+" "+Group, Toast.LENGTH_SHORT).show();

		  WhatIsKurs.setText(sPref.getLong("enterKursText", 1)+" курс "+sPref.getLong("enterGroupText", 1)+" группа");
		mSlidingTabLayout.setViewPager(mViewPager);
	}

	// Adapter
	class SamplePagerAdapter extends PagerAdapter {

		/**
		 * Return the number of pages to display
		 */
		@Override
		public int getCount() {
			return 6;
		}

		/**
		 * Return true if the value returned from is the same object as the View
		 * added to the ViewPager.
		 */
		@Override
		public boolean isViewFromObject(View view, Object o) {
			return o == view;
		}

		/**
		 * Return the title of the item at position. This is important as what
		 * this method returns is what is displayed in the SlidingTabLayout.
		 */
		@Override
		public CharSequence getPageTitle(int position) {
			String[] list = { "Пн", "Вт", "Ср", "Чт", "Пт", "Сб" };

			// return "Item " + (position + 1);

			return list[position];
		}

		/**
		 * Instantiate the View which should be displayed at position. Here we
		 * inflate a layout from the apps resources and then change the text
		 * view to signify the position.
		 */
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			// Inflate a new layout from our resources
			View view = getActivity().getLayoutInflater().inflate(
					R.layout.pager_item, container, false);
			// Add the newly created View to the ViewPager
			container.addView(view);

			// Retrieve a TextView from the inflated View, and update it's text
			// TextView title = (TextView) view.findViewById(R.id.item_title);
			// title.setText(String.valueOf(position + 1));
			// final ImageView image = (ImageView)
			// view.findViewById(R.id.imageView1);
			int positionnew;
			positionnew = position + 1;


		/*	http://remlite.ru/android/timetable/"+Facultet+"/"+Kurs+"/"+Group+"/"
			+ positionnew + ".png*/



//для вывода изображений используем библиотеку Picasso
			Picasso.get().load("http://remlite.ru/android/timetable/"+Facultet+"/"+Kurs+"/"+Group+"/"
			+ positionnew + ".png").into((ImageView) view.findViewById(R.id.imageView1)); //ссылка на ImageView

			// Return the View
			return view;
		}

		/**
		 * Destroy the item from the ViewPager. In our case this is simply
		 * removing the View.
		 */
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}
	}

}

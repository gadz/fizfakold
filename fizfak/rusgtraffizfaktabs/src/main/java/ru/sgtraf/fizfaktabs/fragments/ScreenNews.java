package ru.sgtraf.fizfaktabs.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

//http необходимо заменить на URLConnection

import org.json.JSONArray;
import org.json.JSONObject;

import ru.sgtraf.fizfaktabs.R;
import ru.sgtraf.fizfaktabs.WeekInfo;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


public class ScreenNews extends Fragment {

    protected static final String Tag = "Otvet";
    String line;

    private static final String ZERO = "date";
    private static final String FIRST = "logins";
    private static final String SECOND = "autos";

    private ProgressDialog dialog;
    View rootView;

    public ScreenNews() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.screen_news, container, false);
        return rootView;
    }

    // тут начинаем асинк таск для получения списка журналов с сервера

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub


        new RequestKatalog().execute("http://remlite.ru/android/getnews.php");

        new RequestWeather().execute("http://remlite.ru/android/weather.php");


        super.onCreate(savedInstanceState);
    }

    class RequestKatalog extends AsyncTask<String, String, String> {

        private final String Tag = "frag";

        @Override
        protected String doInBackground(String... params) {

            DefaultHttpClient client = new DefaultHttpClient();
            //делаем свой юзерагент
            client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "from android application");

            // создаем клиента
            try {
                HttpResponse response = client.execute(new HttpGet(params[0])); // пытаемся
                // выполнить GET запрос
                HttpEntity httpEntity = response.getEntity(); // получаем
                // ответ (содержимое страницы вывода)
                line = EntityUtils.toString(httpEntity, "UTF-8");
                //		 Log.d(Tag, line);

            } catch (Exception e) {
              //  Log.i("Request exception", "Exception: " + e.getMessage()); // Oops
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            dialog.dismiss();

            final ArrayList<HashMap<String, Object>> myBooks = new ArrayList<HashMap<String, Object>>();
            ListView listView = (ListView) rootView
                    .findViewById(R.id.listView1);

            try {
                // создали читателя json объектов и отдали ему строку - result
                JSONObject json = new JSONObject(line);
                // дальше находим вход в наш json им является ключевое слово
                // data
                JSONArray urls = json.getJSONArray("data");
                // проходим циклом по всем нашим параметрам
                for (int i = 0; i < urls.length(); i++) {
                    HashMap<String, Object> hm;
                    hm = new HashMap<String, Object>();


                    // читаем что в себе хранит параметр date
                    hm.put(ZERO, urls.getJSONObject(i).getString("date"));

                    // читаем что в себе хранит параметр title
                    hm.put(FIRST, urls.getJSONObject(i).getString("title"));

                    // читаем что в себе хранит параметр body
                    hm.put(SECOND, urls.getJSONObject(i).getString("body"));

                    myBooks.add(hm);

                }

                // дальше добавляем полученные параметры в наш адаптер

                SimpleAdapter adapter = new SimpleAdapter(getActivity(),
                        myBooks, R.layout.list, new String[]{ZERO, FIRST, SECOND},
                        new int[]{R.id.textView1, R.id.text1, R.id.text2});
                // выводим в листвью
                listView.setAdapter(adapter);

            } catch (Exception e) {
              //  Log.e("log_tag", "Error parsing data " + e.toString());
                Toast.makeText(getActivity(), "Нет связи с сервером", Toast.LENGTH_SHORT).show();
            }


            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {

            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Получение...");
            dialog.setIndeterminate(true);
            dialog.setCancelable(true);
            dialog.show();
            super.onPreExecute();
        }
    }

    // запрашиваем погоду с сервера

    class RequestWeather extends AsyncTask<String, String, String> {

        private final String Tag = "frag";

        @Override
        protected String doInBackground(String... params) {

            DefaultHttpClient client = new DefaultHttpClient();
            //делаем свой юзерагент
            client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "from android application");

            // создаем клиента
            try {
                HttpResponse response = client.execute(new HttpGet(params[0])); // пытаемся
                // выполнить GET запрос
                HttpEntity httpEntity = response.getEntity(); // получаем
                // ответ (содержимое страницы вывода)
                line = EntityUtils.toString(httpEntity, "UTF-8");
             //   Log.d(Tag, line);

            } catch (Exception e) {
               // Log.i("Request exception", "Exception: " + e.getMessage()); // Oops
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            final ArrayList<HashMap<String, Object>> myBooks = new ArrayList<HashMap<String, Object>>();

            TextView textViewAir = (TextView) rootView.findViewById(R.id.textView23);
            TextView textViewWater = (TextView) rootView.findViewById(R.id.textView24);


            try {
                // создали читателя json объектов и отдали ему строку - result
                JSONObject json = new JSONObject(line);
                // дальше находим вход в наш json им является ключевое слово
                // data
                JSONArray urls = json.getJSONArray("data");
                // проходим циклом по всем нашим параметрам
                for (int i = 0; i < urls.length(); i++) {
                    HashMap<String, Object> hm;
                    hm = new HashMap<String, Object>();

                    // читаем что в себе хранит параметр date
                    hm.put("temperature", urls.getJSONObject(i).getString("temperature"));

                    // читаем что в себе хранит параметр title
                    hm.put("water_temp", urls.getJSONObject(i).getString("water_temperature"));

                    myBooks.add(hm);

                }

                // дальше добавляем полученные параметры в наш текствью

                textViewAir.setText("Воздух " + myBooks.get(0).get("temperature").toString());
               // textViewWater.setText("Вода " + myBooks.get(0).get("water_temp").toString());
                textViewWater.setText(Integer.toString(WeekInfo.getNumberOfCurrentWeek())+"-ая неделя");

                //    Toast.makeText(getActivity(),  myBooks.get(0).get("temperature").toString() , Toast.LENGTH_SHORT).show();


            } catch (Exception e) {
              //  Log.e("log_tag", "Error parsing data " + e.toString());
                Toast.makeText(getActivity(), "Нет связи с сервером", Toast.LENGTH_SHORT).show();
            }


            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
    }

}
